# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

function(besa_add_clang_tidy TESTNAME)

  set(BESA_CLANG_TIDY_PASSTHROUGH "" CACHE STRING
    "Passthrough Arguments for Clang Tidy")

  find_program(BESA_RUN_CLANG_TIDY run-clang-tidy REQUIRED NO_DEFAULT_PATH
    PATHS ${BESA_BINARY_PATH_ARRAY} DOC "run-clang-tidy executable")

  find_program(BESA_CLANG_TIDY clang-tidy REQUIRED NO_DEFAULT_PATH
    PATHS ${BESA_BINARY_PATH_ARRAY} DOC "clang-tidy executable")

  add_test(
    NAME ${TESTNAME}
    COMMAND ${BESA_RUN_CLANG_TIDY}
    -clang-tidy-binary ${BESA_CLANG_TIDY}
    -extra-arg=${BESA_CLANG_TIDY_PASSTHROUGH}
    -p ${PROJECT_BINARY_DIR}
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    )
endfunction()
