# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

if(NOT CMAKE_CXX_COMPILER_ID STREQUAL CMAKE_C_COMPILER_ID)
  message(FATAL_ERROR "C and C++ Compilers should be the same for coverage to work.")
endif()

target_compile_options(besa::coverage INTERFACE
  $<$<COMPILE_LANGUAGE:C>:-fprofile-instr-generate>
  $<$<COMPILE_LANGUAGE:C>:-fcoverage-mapping>
  $<$<COMPILE_LANGUAGE:CXX>:-fprofile-instr-generate>
  $<$<COMPILE_LANGUAGE:CXX>:-fcoverage-mapping>
  )

target_link_options(besa::coverage INTERFACE
  -fprofile-instr-generate
  -fcoverage-mapping
  )

find_program(BESA_LLVM_PROFDATA llvm-profdata REQUIRED NO_DEFAULT_PATH
  PATHS ${BESA_BINARY_PATH_ARRAY} DOC "llvm-profdata executable"
  )

find_program(BESA_LLVM_COV llvm-cov REQUIRED NO_DEFAULT_PATH
  PATHS ${BESA_BINARY_PATH_ARRAY} DOC "llvm-cov executable"
  )

function(besa_coverage_process_group GROUP_NAME)
  if(NOT TARGET besa.coverage.${GROUP_NAME})
    message(FATAL_ERROR "Unknown Test Coverage Group: ${GROUP_NAME}")
  endif()

  set(PROFILE_FILE_ARRAY)
  foreach(TEST ${BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY})
    list(APPEND PROFILE_FILE_ARRAY
      "${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}/prof/${TEST}.prof")
  endforeach()

  set(TARGET_PATH_ARRAY)
  foreach(TARGET ${BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY})
    list(APPEND TARGET_PATH_ARRAY "-object")
    list(APPEND TARGET_PATH_ARRAY "$<TARGET_FILE:${TARGET}>")

  endforeach()

  add_custom_command(
    OUTPUT ${GROUP_NAME}.profdata
    WORKING_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND ${BESA_LLVM_PROFDATA} merge -sparse ${PROFILE_FILE_ARRAY} -o
    ${GROUP_NAME}.profdata
    DEPENDS ${TARGET_NAME})

  add_custom_command(
    OUTPUT ${GROUP_NAME}.dat
    WORKING_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${BESA_LLVM_COV} show ${TARGET_PATH_ARRAY}
    -instr-profile=${GROUP_NAME}.profdata -Xdemangler=c++filt
    -show-instantiation-summary > ${GROUP_NAME}.dat
    DEPENDS ${GROUP_NAME}.profdata)

  add_custom_command(
    OUTPUT ${GROUP_NAME}.json
    WORKING_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${BESA_LLVM_COV} export ${TARGET_PATH_ARRAY}
    -instr-profile=${GROUP_NAME}.profdata -Xdemangler=c++filt >
    ${GROUP_NAME}.json
    DEPENDS ${GROUP_NAME}.profdata)

  add_custom_command(
    OUTPUT ${GROUP_NAME}.html
    WORKING_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${BESA_LLVM_COV} show ${TARGET_PATH_ARRAY}
    -instr-profile=${GROUP_NAME}.profdata -Xdemangler=c++filt --format=html
    -region-coverage-lt=100 > ${GROUP_NAME}.html
    DEPENDS ${GROUP_NAME}.profdata)

  add_custom_target(
    besa.coverage.${GROUP_NAME}.files
    DEPENDS ${GROUP_NAME}.dat ${GROUP_NAME}.html ${GROUP_NAME}.json
    WORKING_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR})

  add_dependencies(besa.coverage.${GROUP_NAME}
    besa.coverage.${GROUP_NAME}.files)
endfunction()

function(besa_coverage_initialize_group GROUP_NAME)
  add_custom_target(besa.coverage.${GROUP_NAME})
  add_dependencies(besa.coverage besa.coverage.${GROUP_NAME})

  set(BESA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL
    ${BESA_COVERAGE_OUTDIR}/${GROUP_NAME})

  set(BESA_COVERAGE_${GROUP_NAME}_OUTDIR
    ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}
    CACHE INTERNAL "${GROUP_NAME} Output Directory")

  set(BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY CACHE INTERNAL
    "${GROUP_NAME} Test Array")

  set(BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
    CACHE INTERNAL "${GROUP_NAME} Target Array")

  file(MAKE_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}/prof)

  file(WRITE ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}/compiler "Clang\n")

  cmake_language(
    EVAL CODE "cmake_language(DEFER CALL besa_coverage_process_group [[${GROUP_NAME}]])")
endfunction()

function(besa_coverage_register_test GROUP_NAME TEST_NAME TARGET_NAME)
  if(NOT TARGET besa.coverage.${GROUP_NAME})
    besa_coverage_initialize_group(${GROUP_NAME})
  endif()

  set_tests_properties(
    ${TEST_NAME}
    PROPERTIES
    ENVIRONMENT
    LLVM_PROFILE_FILE=${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}/prof/${TEST_NAME}.prof
    )

  list(APPEND BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY "${TEST_NAME}")

  set(BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY
    ${BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY}
    CACHE INTERNAL "${GROUP_NAME} Test Array")

  list(APPEND BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY "${TARGET_NAME}")

  set(BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
    ${BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY}
    CACHE INTERNAL "${GROUP_NAME} Test Array")
endfunction()
