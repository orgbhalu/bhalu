# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

if(NOT CMAKE_CXX_COMPILER_ID STREQUAL CMAKE_C_COMPILER_ID)
  message(FATAL_ERROR "besa Coverage needs C and C++ Compilers to be identical.")
endif()

target_compile_options(besa::coverage INTERFACE
  $<$<COMPILE_LANGUAGE:C>:-fprofile-arcs>
  $<$<COMPILE_LANGUAGE:C>:-ftest-coverage>
  $<$<COMPILE_LANGUAGE:C>:-fprofile-update=atomic>
  $<$<COMPILE_LANGUAGE:CXX>:-fprofile-arcs>
  $<$<COMPILE_LANGUAGE:CXX>:-ftest-coverage>
  $<$<COMPILE_LANGUAGE:CXX>:-fprofile-update=atomic>
  )

target_link_options(besa::coverage INTERFACE --coverage)

find_program(BESA_LCOV lcov REQUIRED NO_DEFAULT_PATH
  PATHS ${BESA_BINARY_PATH_ARRAY} DOC "lcov executable")

find_program(BESA_GENHTML genhtml REQUIRED NO_DEFAULT_PATH
  PATHS ${BESA_BINARY_PATH_ARRAY} DOC "genhtml executable")

function(besa_coverage_process_group GROUP_NAME)
  if(NOT TARGET besa.coverage.${GROUP_NAME})
    message(FATAL_ERROR "Unknown Test Coverage Group: ${GROUP_NAME}")
  endif()

  add_custom_command(
    OUTPUT ${GROUP_NAME}.info
    WORKING_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}
    COMMAND
    ${CMAKE_COMMAND} -DCOVERAGE_OUTDIR=${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}
    -DGROUP_NAME=${GROUP_NAME} -DBESA_LCOV=${BESA_LCOV}
    -DBESA_GENHTML=${BESA_GENHTML} -P
    ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/gcc.impl.cmake)

  add_custom_target(besa.coverage.${GROUP_NAME}.files DEPENDS ${GROUP_NAME}.info
    WORKING_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR})

  add_dependencies(besa.coverage.${GROUP_NAME}
    besa.coverage.${GROUP_NAME}.files)
endfunction()

function(besa_coverage_initialize_group GROUP_NAME)
  add_custom_target(besa.coverage.${GROUP_NAME})
  add_dependencies(besa.coverage besa.coverage.${GROUP_NAME})

  set(BESA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL
    ${BESA_COVERAGE_OUTDIR}/${GROUP_NAME})

  set(BESA_COVERAGE_${GROUP_NAME}_OUTDIR
    ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}
    CACHE INTERNAL "${GROUP_NAME} Output Directory")

  set(BESA_COVERAGE_${GROUP_NAME}_GCDA_DIR
    ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR}/gcda/
    CACHE INTERNAL "${GROUP_NAME} GCDA Output Directory")

  set(BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY CACHE INTERNAL
    "${GROUP_NAME} Test Array")

  set(BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
    CACHE INTERNAL "${GROUP_NAME} Target Array")

  file(MAKE_DIRECTORY ${BESA_COVERAGE_${GROUP_NAME}_OUTDIR_LOCAL}/prof)

  cmake_language(
    EVAL CODE "cmake_language(DEFER CALL besa_coverage_process_group [[${GROUP_NAME}]])")
endfunction()

function(besa_coverage_register_test GROUP_NAME TEST_NAME TARGET_NAME)
  if(NOT TARGET besa.coverage.${GROUP_NAME})
    besa_coverage_initialize_group(${GROUP_NAME})
  endif()

  set_tests_properties(
    ${TEST_NAME} PROPERTIES ENVIRONMENT
    GCOV_PREFIX=${BESA_COVERAGE_${GROUP_NAME}_GCDA_DIR})

  list(APPEND BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY "${TEST_NAME}")

  set(BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY
    ${BESA_COVERAGE_${GROUP_NAME}_TEST_ARRAY}
    CACHE INTERNAL "${GROUP_NAME} Test Array")

  list(APPEND BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY "${TARGET_NAME}")

  set(BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY
    ${BESA_COVERAGE_${GROUP_NAME}_TARGET_ARRAY}
    CACHE INTERNAL "${GROUP_NAME} Test Array")
endfunction()
