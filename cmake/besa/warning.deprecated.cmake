# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

add_library(besa::warning.deprecated INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::warning.deprecated INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wdeprecated>)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options(besa::warning.deprecated INTERFACE $<$<COMPILE_LANGUAGE:C>:-Wdeprecated>)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::warning.deprecated INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wdeprecated>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(besa::warning.deprecated INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Wdeprecated>)
endif()
