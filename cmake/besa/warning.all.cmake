# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

add_library(besa::warning.all INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::warning.all INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-Weverything>
    $<$<COMPILE_LANGUAGE:C>:-Wno-c++98-compat>
    $<$<COMPILE_LANGUAGE:C>:-Wno-c++20-compat>
    $<$<COMPILE_LANGUAGE:C>:-Wno-c++98-compat-pedantic>
    $<$<COMPILE_LANGUAGE:C>:-Wno-covered-switch-default>
    $<$<COMPILE_LANGUAGE:C>:-Wno-padded>
    $<$<COMPILE_LANGUAGE:C>:-Wno-weak-vtables>
    $<$<COMPILE_LANGUAGE:CXX>:-Weverything>
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-c++98-compat>
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-c++20-compat>
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-c++98-compat-pedantic>
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-covered-switch-default>
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-padded>
    $<$<COMPILE_LANGUAGE:CXX>:-Wno-weak-vtables>
    )
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options(besa::warning.all INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-Wall>
    $<$<COMPILE_LANGUAGE:C>:-Wextra>
    $<$<COMPILE_LANGUAGE:C>:-Wpedantic>
    $<$<COMPILE_LANGUAGE:CXX>:-Wall>
    $<$<COMPILE_LANGUAGE:CXX>:-Wextra>
    $<$<COMPILE_LANGUAGE:CXX>:-Wpedantic>
    )
endif()
