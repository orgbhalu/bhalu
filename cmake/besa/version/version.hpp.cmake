// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-FileCopyrightText: (C) 2023 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <string_view>

namespace @VERSION_NAMESPACE@ {
class version_t {
public:
  version_t() = default;

public:
  constexpr std::size_t major() const noexcept;
  constexpr std::size_t minor() const noexcept;
  constexpr std::size_t patch() const noexcept;
  constexpr std::size_t tweak() const noexcept;

  constexpr std::string_view pkgbuilder_id() const noexcept;
  constexpr std::size_t pkgbuilder_revision() const noexcept;

  constexpr std::string_view release_id() const noexcept;
  constexpr std::size_t release_revision() const noexcept;

private:
  // Semantic versioning
  std::size_t major_{@PROJECT_VERSION_MAJOR@};
  std::size_t minor_{@PROJECT_VERSION_MINOR@};
  std::size_t patch_{@PROJECT_VERSION_PATCH@};
  std::size_t tweak_{@PROJECT_VERSION_TWEAK@};

  // Packager Information
  std::string_view pkgbuilder_id_{@PKGBUILDER_ID@};
  std::size_t pkgbuilder_revision_{@PKGBUILDER_REVISION@};

  // Build Information
  std::string_view release_id_{@RELEASE_ID@};
  std::size_t release_revision_{@RELEASE_REVISION@};
};

constexpr std::size_t version_t::major() const noexcept {
  return major_;
}

constexpr std::size_t version_t::minor() const noexcept {
  return minor_;
}

constexpr std::size_t version_t::patch() const noexcept {
  return patch_;
}

constexpr std::size_t version_t::tweak() const noexcept {
  return tweak_;
}

constexpr std::string_view version_t::pkgbuilder_id() const noexcept {
  return pkgbuilder_id_;
}

constexpr std::size_t version_t::pkgbuilder_revision() const noexcept {
  return pkgbuilder_revision_;
}

constexpr std::string_view version_t::release_id() const noexcept {
  return release_id_;
}

constexpr std::size_t version_t::release_revision() const noexcept {
  return release_revision_;
}
} // namespace @VERSION_NAMESPACE@
