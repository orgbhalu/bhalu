# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(besa::asan INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::asan INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-fsanitize=address>
    $<$<COMPILE_LANGUAGE:C>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:C>:-fno-sanitize-recover=address>
    $<$<COMPILE_LANGUAGE:C>:-fno-optimize-sibling-calls>
    $<$<COMPILE_LANGUAGE:C>:-fsanitize-address-use-after-scope>
    )

  target_link_options(besa::asan INTERFACE
    -fsanitize=address
    -fno-omit-frame-pointer
    -fno-sanitize-recover=address
    -fno-optimize-sibling-calls
    -fsanitize-address-use-after-scope)


elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")

  target_compile_options(besa::asan INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-fsanitize=address>
    $<$<COMPILE_LANGUAGE:C>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:C>:-fno-sanitize-recover=address>
    $<$<COMPILE_LANGUAGE:C>:-fno-optimize-sibling-calls>
    $<$<COMPILE_LANGUAGE:C>:-fsanitize-address-use-after-scope>
    )

  target_link_options(besa::asan INTERFACE
    -fsanitize=address
    -fno-omit-frame-pointer
    -fno-sanitize-recover=address
    -fno-optimize-sibling-calls
    -fsanitize-address-use-after-scope)

endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::asan INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=address>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-sanitize-recover=address>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-optimize-sibling-calls>
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize-address-use-after-scope>
    )

  target_link_options(besa::asan INTERFACE
    -fsanitize=address
    -fno-omit-frame-pointer
    -fno-sanitize-recover=address
    -fno-optimize-sibling-calls
    -fsanitize-address-use-after-scope)


elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

  target_compile_options(besa::asan INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=address>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-omit-frame-pointer>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-sanitize-recover=address>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-optimize-sibling-calls>
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize-address-use-after-scope>
    )

  target_link_options(besa::asan INTERFACE
    -fsanitize=address
    -fno-omit-frame-pointer
    -fno-sanitize-recover=address
    -fno-optimize-sibling-calls
    -fsanitize-address-use-after-scope)


endif()
