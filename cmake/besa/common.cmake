# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

set(BESA_BINARY_PATH_ARRAY ${CMAKE_SYSTEM_PREFIX_PATH})
list(PREPEND BESA_BINARY_PATH_ARRAY ${CMAKE_PREFIX_PATH})
list(TRANSFORM BESA_BINARY_PATH_ARRAY APPEND "/bin")

