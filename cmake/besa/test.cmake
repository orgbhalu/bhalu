# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
#--------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

include(${CMAKE_CURRENT_LIST_DIR}/coverage.cmake)

function(besa_internal_unit_test_name FILENAME PREFIX TARGETNAME)
  # Remove the extension from the source file name
  string(REGEX REPLACE "\\.[^.]*$" "" TARGETNAME ${SOURCE})
  set(TARGETNAME "${PREFIX}.${TARGETNAME}")
  set(TARGETNAME "${TARGETNAME}" PARENT_SCOPE)
endfunction()

function(besa_add_test_directory
    DIRECTORY_LITERAL DIR_NAME
    TARGET_LIST_LITERAL TARGET_LIST
    PREFIX_LITERAL PREFIX
    LABELS_LITERAL LABELNAME
    COVERAGE_LITERAL COVERAGE
    COVERAGE_GROUP_LITERAL COVERAGE
    CMDLINE_LITERAL CMDLINE
    )

  if(NOT DIRECTORY_LITERAL STREQUAL "DIRECTORY"
      OR NOT TARGET_LIST_LITERAL STREQUAL "TARGET_LIST"
      OR NOT PREFIX_LITERAL STREQUAL "PREFIX"
      OR NOT LABELS_LITERAL STREQUAL "LABELS"
      OR NOT COVERAGE_LITERAL STREQUAL "COVERAGE"
      OR NOT COVERAGE_GROUP_LITERAL STREQUAL "COVERAGE_GROUP"
      OR NOT CMDLINE_LITERAL STREQUAL "CMDLINE"
      )

    message(FATAL_ERROR "Incorrect usage of add_test_directory
    DIRECTORY_LITERAL = ${DIRECTORY_LITERAL}
    TARGET_LIST_LITERAL = ${TARGET_LIST_LITERAL}
    PREFIX_LITERAL = ${PREFIX_LITERAL}
    LABELS_LITERAL = ${LABELS_LITERAL}
    COVERAGE_LITERAL = ${COVERAGE_LITERAL}
    COVERAGE_GROUP_LITERAL = ${COVERAGE_GROUP_LITERAL}
    CMDLINE_LITERAL = ${CMDLINE_LITERAL}
    ")

  endif()

  file(REAL_PATH ${DIR_NAME} ABS_DIR_NAME BASE_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})

  file(GLOB SOURCE_LIST LIST_DIRECTORIES false
    RELATIVE ${ABS_DIR_NAME} CONFIGURE_DEPENDS ${DIR_NAME}/*)

  foreach(SOURCE ${SOURCE_LIST})
    besa_internal_unit_test_name(${SOURCE} ${PREFIX} TARGETNAME)

    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${SOURCE})
    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${TESTDIRNAME})
    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${TESTDIRNAME})

    add_executable(${TARGETNAME} ${ABS_DIR_NAME}/${SOURCE})
    besa_target_source_directory(${TARGETNAME} PRIVATE ${ABS_DIR_NAME}/${TESTDIRNAME})

    add_test(NAME ${TARGETNAME} COMMAND ${TARGETNAME} WORKING_DIRECTORY ${PROJECT_BINARY_DIR})

    set_property(TEST ${TARGETNAME} PROPERTY LABELS ${LABELNAME})

    foreach(TARGET_LIB ${TARGET_LIB_LIST})
      target_link_libraries(${TARGETNAME} PRIVATE ${TARGET_LIB})
    endforeach()

    if(TARGETNAME MATCHES ".fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES WILL_FAIL TRUE)
    endif()

    if(TARGETNAME MATCHES "disabled.t$|disabled.fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    if(COVERAGE STREQUAL "TRUE")
      besa_coverage_register_test(${COVERAGE_GROUP} ${TARGETNAME} ${TARGETNAME})
    endif()

  endforeach()
endfunction()

if(BESA_NOPREFIX)
  function(add_test_directory
      DIRECTORY_LITERAL DIR_NAME
      TARGET_LIST_LITERAL TARGET_LIST
      PREFIX_LITERAL PREFIX
      LABELS_LITERAL LABELNAME
      COVERAGE_LITERAL COVERAGE
      COVERAGE_GROUP_LITERAL COVERAGE
      CMDLINE_LITERAL CMDLINE
      )

    besa_add_test_directory(
      "${DIRECTORY_LITERAL}" "${DIR_NAME}"
      "${TARGET_LIST_LITERAL}" "${TARGET_LIST}"
      "${PREFIX_LITERAL}" "${PREFIX}"
      "${LABELS_LITERAL}" "${LABELNAME}"
      "${COVERAGE_LITERAL}" "${COVERAGE}"
      "${COVERAGE_GROUP_LITERAL}" "${COVERAGE}"
      "${CMDLINE_LITERAL}" "${CMDLINE}"
      )
  endfunction()
endif()



function(besa_add_compile_test_directory DIR_NAME TARGET_LIB_LIST PREFIX LABELNAME)
  file(REAL_PATH ${DIR_NAME} ABS_DIR_NAME BASE_DIRECTORY ${CMAKE_CURRENT_LIST_DIR})
  file(
    GLOB SOURCE_FILE_LIST LIST_DIRECTORIES false
    RELATIVE ${ABS_DIR_NAME} CONFIGURE_DEPENDS ${DIR_NAME}/*)

  file(RELATIVE_PATH REL_DIR_NAME ${CMAKE_CURRENT_LIST_DIR} ${ABS_DIR_NAME})


  foreach(SRC_FILE ${SOURCE_FILE_LIST})
    string(REGEX REPLACE "\\.[^.]*$" "" TARGETNAME ${SRC_FILE})
    string(REGEX REPLACE "/" "." TARGETNAME ${TARGETNAME})
    set(TARGETNAME "${PREFIX}.${REL_DIR_NAME}.${TARGETNAME}")
    add_executable(${TARGETNAME} ${ABS_DIR_NAME}/${SRC_FILE})

    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${SRC_FILE})
    string(REGEX REPLACE "\\.[^.]*$" "" TESTDIRNAME ${TESTDIRNAME})

    foreach(TARGET_LIB IN LISTS TARGET_LIB_LIST)
      target_link_libraries(${TARGETNAME} PRIVATE ${TARGET_LIB})
    endforeach()
    set_target_properties(
      ${TARGETNAME} PROPERTIES EXCLUDE_FROM_ALL TRUE EXCLUDE_FROM_DEFAULT_BUILD
      TRUE)

    add_test(NAME ${TARGETNAME} COMMAND ${CMAKE_COMMAND} --build . --target
      ${TARGETNAME} --config $<CONFIG>
      WORKING_DIRECTORY ${PROJECT_BINARY_DIR})

    set_property(TEST ${TARGETNAME} PROPERTY LABELS ${LABELNAME})

    if(TARGETNAME MATCHES ".fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES WILL_FAIL TRUE)
    endif()
    if(TARGETNAME MATCHES "disabled.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    if(TARGETNAME MATCHES "disabled.fail.t$")
      set_tests_properties(${TARGETNAME} PROPERTIES DISABLED TRUE)
    endif()

    besa_target_source_directory(${TARGETNAME} PRIVATE ${REL_DIR_NAME}/${TESTDIRNAME})
  endforeach()
endfunction()

if(BESA_NOPREFIX)
  function(add_compile_test_directory DIR_NAME TARGET_LIB_LIST PREFIX LABELNAME)
    besa_add_compile_test_directory(
      "${DIR_NAME}" "${TARGET_LIB_LIST}" "${PREFIX}" "${LABELNAME}"
      )
  endfunction()
endif()

