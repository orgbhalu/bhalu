# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include(CMakePushCheckState)
cmake_push_check_state()

include(${CMAKE_CURRENT_LIST_DIR}/common.cmake)

set(CMAKE_REQUIRED_QUIET ${besa_FIND_QUIETLY})
set(want_components ${besa_FIND_COMPONENTS})

set(extra_components ${want_components})

message(STATUS "Looking for besa components: ${want_components}")
list(
  REMOVE_ITEM extra_components
  asan builder tidy coverage doxygen explorer feature-set lsan mode surrogate test ubsan version
  warning.all warning.deprecated warning.error warning.essential
  )

foreach(component IN LISTS extra_components)
  message(FATAL_ERROR "Invalid find_package component for besa: ${component}")
endforeach()

foreach(component IN LISTS want_components)
  include(${CMAKE_CURRENT_LIST_DIR}/${component}.cmake)
endforeach()

cmake_pop_check_state()
