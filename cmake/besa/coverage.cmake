# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(besa::coverage INTERFACE IMPORTED)

add_custom_target(tora.coverage)

set(BESA_COVERAGE_OUTDIR ${PROJECT_BINARY_DIR}/coverage
  CACHE PATH "Path to Store Output of Coverage Data")

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/clang.cmake)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/gcc.cmake)
else()
  message(FATAL_ERROR "besa coverage doesn't support compiler: ${CMAKE_CXX_COMPILER_ID}")
endif()

