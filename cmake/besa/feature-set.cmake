# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

function(besa_feature_set_internal_check_validity FEATURE_SET_NAME)
  set(FEATURE_SET_VALUE ${${FEATURE_SET_NAME}})

  foreach(FEATURE ${FEATURE_SET_VALUE})
    if(NOT ${FEATURE} IN_LIST ${FEATURE_SET_NAME}_ARRAY)
      message(
        FATAL_ERROR
        "
        Invalid ${FEATURE_SET_NAME} Feature ${FEATURE}.
        Valid values are: none,${${FEATURE_SET_NAME}_ARRAY},all
        "
        )
    endif()
  endforeach()
endfunction()

function(besa_feature_set_help)
  message("Incorrect Usage of besa_feature_set. Correct Usage is:")
  message(
    FATAL_ERROR
    "
    besa_feature_set(
      <NAME>
      VALUES <ARRAY_OF_SUPPORTED_VALUES>
      DESCRIPTION <DESCRIPTION>
      )
    "
    )
endfunction()

function(
    besa_feature_set
    FEATURE_SET_NAME
    VALUES_NAME FEATURE_SET_ARRAY
    DESCRIPTION
    )

  if(NOT (${VALUES_NAME} STREQUAL "VALUES"))
    besa_feature_set_help()
  endif()

  set(${FEATURE_SET_NAME} ${${FEATURE_SET_NAME}} PARENT_SCOPE)

  set(${FEATURE_SET_NAME}_ARRAY ${FEATURE_SET_ARRAY})
  set(${FEATURE_SET_NAME}_ARRAY ${${FEATURE_SET_NAME}_ARRAY} PARENT_SCOPE)

  if("none" IN_LIST ${FEATURE_SET_NAME})

    list(LENGTH ${FEATURE_SET_NAME} FEATURE_LENGTH)
    if(NOT (${FEATURE_LENGTH} EQUAL 1))
      message(
        FATAL_ERROR
        "
        none is mutually exclusive with other ${FEATURE_SET_NAME} features.
        "
        )

    endif()

  elseif("all" IN_LIST ${FEATURE_SET_NAME})

    list(LENGTH ${FEATURE_SET_NAME} FEATURE_LENGTH)
    if(NOT (${FEATURE_LENGTH} EQUAL 1))

      message(
        FATAL_ERROR
        "
        all is mutually exclusive with other ${FEATURE_SET_NAME} features.
        "
        )
    endif()

    foreach(FEATURE ${FEATURE_SET_ARRAY})
      string(TOUPPER ${FEATURE} FEATURE_UPPER)
      string(REPLACE "-" "_" FEATURE_UPPER ${FEATURE_UPPER})
      set(FEATURE_VARIABLE ${FEATURE_SET_NAME}_${FEATURE_UPPER})
      set(${FEATURE_VARIABLE} TRUE PARENT_SCOPE)
    endforeach()

  else()
    besa_feature_set_internal_check_validity(${FEATURE_SET_NAME})

    foreach(FEATURE ${FEATURE_SET_ARRAY})
      string(TOUPPER ${FEATURE} FEATURE_UPPER)
      string(REPLACE "-" "_" FEATURE_UPPER ${FEATURE_UPPER})
      set(FEATURE_VARIABLE ${FEATURE_SET_NAME}_${FEATURE_UPPER})
      set(${FEATURE_VARIABLE} FALSE PARENT_SCOPE)
    endforeach()

    foreach(FEATURE ${${FEATURE_SET_NAME}})
      string(TOUPPER ${FEATURE} FEATURE_UPPER)
      string(REPLACE "-" "_" FEATURE_UPPER ${FEATURE_UPPER})
      set(FEATURE_VARIABLE ${FEATURE_SET_NAME}_${FEATURE_UPPER})
      set(${FEATURE_VARIABLE} TRUE PARENT_SCOPE)
    endforeach()
  endif()

endfunction()
