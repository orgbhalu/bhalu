# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

add_library(besa::warning.error INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::warning.error INTERFACE $<$<COMPILE_LANGUAGE:C>:-Werror>)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options(besa::warning.error INTERFACE $<$<COMPILE_LANGUAGE:C>:-Werror>)
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::warning.error INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Werror>)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(besa::warning.error INTERFACE $<$<COMPILE_LANGUAGE:CXX>:-Werror>)
endif()
