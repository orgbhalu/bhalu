# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)

add_library(besa::lsan INTERFACE IMPORTED)

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::lsan INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-fsanitize=leak>
    )

  target_link_options(besa::lsan INTERFACE
    -fsanitize=leak
    )


elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")

  target_compile_options(besa::lsan INTERFACE
    $<$<COMPILE_LANGUAGE:C>:-fsanitize=leak>
    )

  target_link_options(besa::lsan INTERFACE
    -fsanitize=leak
    )

endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  target_compile_options(besa::lsan INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=leak>
    )

  target_link_options(besa::lsan INTERFACE
    -fsanitize=leak
    )


elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

  target_compile_options(besa::lsan INTERFACE
    $<$<COMPILE_LANGUAGE:CXX>:-fsanitize=leak>
    )

  target_link_options(besa::lsan INTERFACE
    -fsanitize=leak
    )


endif()
