// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

auto f(int a, int b) -> int;

auto f(int a, int b) -> int
{
  return a + b;
}
