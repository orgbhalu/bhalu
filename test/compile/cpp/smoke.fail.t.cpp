// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#ifndef BHALU_DISABLE_CLANG_TIDY

#include <cstdlib>

auto main() -> int
}

#endif // BHALU_DISABLE_CLANG_TIDY
