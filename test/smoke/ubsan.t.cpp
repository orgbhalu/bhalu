// -----------------------------------------------------------------------------
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -----------------------------------------------------------------------------

// NOLINTNEXTLINE
int main(int argc, char** argv)
{
  // NOLINTNEXTLINE
  int k = 0x7fffffff;
  // NOLINTNEXTLINE
  k += argc;
  return 0;
}
