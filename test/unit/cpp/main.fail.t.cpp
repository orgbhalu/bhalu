// -----------------------------------------------------------------------------
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-FileCopyrightText: (C) 2021-2021, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -----------------------------------------------------------------------------

#include <cstdlib>

auto alpha() -> int;

auto alpha() -> int
{
  return 0;
}

auto main() -> int
{
  return EXIT_FAILURE;
}
