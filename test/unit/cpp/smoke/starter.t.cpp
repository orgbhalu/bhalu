// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: AGPL-3.0-only
// SPDX-FileCopyrightText: (C) 2022-2022, Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <cstdlib>

auto func() -> int;

auto func() -> int
{
  return EXIT_SUCCESS;
}
