# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(BESA_NOPREFIX TRUE)

add_library(bhalu::internal INTERFACE IMPORTED)

include(GNUInstallDirs)
include(CTest)
find_package(besa REQUIRED COMPONENTS version)
find_package(besa REQUIRED COMPONENTS feature-set)
find_package(besa REQUIRED COMPONENTS mode)
find_package(besa REQUIRED COMPONENTS builder)
find_package(besa REQUIRED COMPONENTS test)
find_package(besa REQUIRED COMPONENTS explorer)
find_package(besa REQUIRED COMPONENTS surrogate)


