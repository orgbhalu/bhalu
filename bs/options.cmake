# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2021-2021, Jayesh Badwaik <jayesh@badwaik.dev>
# --------------------------------------------------------------------------------------------------

include(CMakeDependentOption)


# --------------------------------------------------------------------------------------------------
# Options
# --------------------------------------------------------------------------------------------------
option(BUILD_DOC "Build Documentation" ON)
option(BUILD_SOURCE "Build Source" ON)
option(BUILD_TESTING "Build Tests" OFF)
option(WARNING_DEPRECATED "Enable Deprecated Warnings" OFF)
option(WARNING_AS_ERROR "Enable Error Warnings" OFF)

# --------------------------------------------------------------------------------------------------
# Features
# --------------------------------------------------------------------------------------------------
set(LINT_FEATURE "none" CACHE STRING "Lint Features")
set(LINT_FEATURE_SET "clang-tidy;format;git-format" STRING "Supported Lint Features")

# --------------------------------------------------------------------------------------------------
# Modes
# --------------------------------------------------------------------------------------------------
set(INSTRUMENT_MODE "none" CACHE STRING "Instrument Features")
set(INSTRUMENT_MODE_SET "none;coverage;asan;lsan;ubsan" STRING "Supported Instrument Features")

set(WARNING_MODE "none" CACHE STRING "Warning Mode")
set(WARNING_MODE_SET "none;essential;everything" STRING "Supported Warning Modes")

# --------------------------------------------------------------------------------------------------
#  Packager Options
# --------------------------------------------------------------------------------------------------
set(PKGBUILDER_ID        "unknown" CACHE STRING "ID for a Package Maintainer")
set(PKGBUILDER_REVISION  "1"       CACHE STRING "Revision of Release")

set(RELEASE_TYPE          "dev"     CACHE STRING "Type of Release")
set(RELEASE_TYPE_SET "dev;release" STRING "Supported Release Types")

set(RELEASE_REVISION      "1"       CACHE STRING "Revision of Release")


