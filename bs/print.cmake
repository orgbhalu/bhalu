# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

message(STATUS "Testing           -     ${BUILD_TESTING}")
message(STATUS "Linting           -     ${LINT_FEATURE}")
message(STATUS "Warnings          -     ${WARNING_MODE}")
message(STATUS "Warning as Error  -     ${WARNING_AS_ERROR}")
message(STATUS "Warn Deprecated   -     ${WARNING_DEPRECATED}")
message(STATUS "Instrument        -     ${INSTRUMENT_MODE}")


message(STATUS "Package Builder ID        - ${PKGBUILDER_ID}")
message(STATUS "Package Builder Revision  - ${PKGBUILDER_REVISION}")
message(STATUS "Release Type              - ${RELEASE_TYPE}")
message(STATUS "Release Revision          - ${RELEASE_REVISION}")

