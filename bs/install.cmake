# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
if(BUILD_SOURCE)
  install(TARGETS libbhalu EXPORT bhaluTarget FILE_SET HEADERS)

  install(FILES ${PROJECT_BINARY_DIR}/lib/bhaluConfigVersion.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/bhalu/cmake/)

  install(FILES ${PROJECT_BINARY_DIR}/lib/bhaluConfig.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/bhalu/cmake)

  install(
    EXPORT bhaluTarget
    FILE bhaluTarget.cmake
    NAMESPACE bhalu::
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/bhalu/cmake)
endif()


