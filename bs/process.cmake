# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

besa_compute_version()

if(BUILD_DOC)
  find_package(besa REQUIRED COMPONENTS doxygen)
  add_custom_target(documentation)
  set_target_properties(documentation PROPERTIES EXCLUDE_FROM_ALL False)

  besa_configure_doxygen(
    documentation
    ${PROJECT_BINARY_DIR}/documentation/doxygen
    ${PROJECT_SOURCE_DIR}/doc/doxygen/doxyfile.in
    )
endif()

if(BUILD_TESTING)
  find_package(Catch2 REQUIRED)
endif()

if (WARNING_DEPRECATED)
  find_package(besa REQUIRED COMPONENTS warning.deprecated)
endif()

if (WARNING_AS_ERROR)
  find_package(besa REQUIRED COMPONENTS warning.error)
endif()

besa_feature_set(LINT_FEATURE VALUES "${LINT_FEATURE_SET}" DESCRIPTION "Lint Features")
if(LINT_FEATURE_CLANG_TIDY)
  find_package(besa REQUIRED COMPONENTS clang-tidy)
endif()

if(LINT_FEATURE_FORMAT)
  find_package(besa REQUIRED COMPONENTS clang-format)
endif()

if(LINT_FEATURE_GIT_FORMAT)
  find_package(besa REQUIRED COMPONENTS git-clang-format)
endif()

besa_mode(INSTRUMENT_MODE VALUES "${INSTRUMENT_MODE_SET}" DESCRIPTION "Supported Instruments")

if(INSTRUMENT_MODE_ASAN)
  find_package(besa REQUIRED COMPONENTS asan)
endif()

if(INSTRUMENT_MODE_LSAN)
  find_package(besa REQUIRED COMPONENTS lsan)
endif()

if(INSTRUMENT_MODE_UBSAN)
  find_package(besa REQUIRED COMPONENTS ubsan)
endif()

besa_mode(WARNING_MODE VALUES "${WARNING_MODE_SET}" DESCRIPTION "Supported Warning Modes")

if(WARNING_MODE STREQUAL "everything")
  find_package(besa REQUIRED COMPONENTS warning.all)
elseif(WARNING_MODE STREQUAL "essential")
  find_package(besa REQUIRED COMPONENTS warning.essential)
endif()




